{
  pkgs,
  nixpkgs,
  ...
}: {
  imports = [
#    ../../../modules/home/protonmail-bridge/default.nix
  ];

  home.username = "mringelborn";
  home.homeDirectory = "/home/mringelborn";

  nixpkgs.config.allowUnfree = true;
  
  services.protonmail-bridge = {
    enable = true;
    nonInteractive = true;
    logLevel = "debug";
  };
  
  programs.git = {
    enable = true;
    userName  = "Marcus Ingelborn";
    userEmail = "git@mringelborn.com";
  };

  programs.vscode = {
    enable = true;
    extensions = with pkgs.vscode-extensions; [
      dracula-theme.theme-dracula
      vscodevim.vim
      yzhang.markdown-all-in-one
    ];
  };

  dconf.settings = {
    "org/gnome/mutter" = {
      experimental-features = [ "scale-monitor-framebuffer" ];
    };
  };
  
  programs.home-manager.enable = true;
  
  home.stateVersion = "23.11";
}
