
build:
	nixos-rebuild build --flake . --impure

install:
	nixos-rebuild switch --flake . --impure --use-remote-sudo

clean:
	rm result

update:
	nix flake update --commit-lock-file

diff:
	nvd diff /run/current-system result
