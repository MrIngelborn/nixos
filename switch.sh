#!/bin/sh

sh build.sh

sudo nixos-rebuild switch --flake .

nix-shell -p home-manager --run "home-manager switch --flake ."
