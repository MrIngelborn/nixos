#!/bin/bash

# Diff system versio 11 to 12
diff -Naur <( nix-store -qR /nix/var/nix/profiles/system-11-link | cut -c45- | sort ) <( nix-store -qR /nix/var/nix/profiles/system-12-link | cut -c45- | sort )
